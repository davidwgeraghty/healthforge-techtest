angular.module('HealthForge', [])
.controller('MainCtrl', function($scope, $http) {

	$scope.sortType = 'lastName';
    $scope.sortReverse = false;
    $scope.searchPatients = '';
    $scope.patients = [];

	// get patient data
	$http.get("https://api.interview.healthforge.io/api/patient")
	.then(function(response) {
		// dynamically alters html element once loaded
		$scope.patients = response.data.content;
	});
});